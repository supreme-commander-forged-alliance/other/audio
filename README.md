# Audio

All kind of information related to making audio files.

## Tool

You can find a zipped version of the tool inside the tool folder. If you do not trust this executable then you can find the original tool in this archive:
 - https://archive.org/details/dxsdk_aug2007

The executable is a winrar self-extractor. It unpacks the data used for installation. Where you unpack this is not relevant. Throughout the installation it will prompt that the prerelease is outdated. Mindfully press 'ok' each time to ignore it.

After installation we are interested in the 'Microsoft Cross-platform audio creation tool (XACT)'. You should be able to find it by searching for it like you would for any other program.

## Vanilla

This is the wavebank information (without the audio files) of the original game. You can use it to study the approach of the game. The audio files themselves are not part of this repository and I don't have them either.

## Template

This is a template that you can use that is essentially the vanilla project file but then with no wave or sound banks in it.

## Example

This is an example of sound added to the mod 'King of the Hill' that is available in the [FAF](https://www.faforever.com/) vault. We'll go through this example to learn how to construct our own audio banks.

I assume that you have cloned or downloaded this repository. From now on forwards when I write `repository/example/wav` then you should append the path to the repository accordingly on your local machine. 

### Files

There are three file types that are relevant:
 - KingOfTheHill.xwb: Xact Wavebank (output of build)
 - KingOfTheHill.xsb: Xact Soundbank (output of build)
 - KingOfTheHill.xap: Xact Application

In your case the name of the files will likely be different. The wavebank contains the actual audio files. The soundbank contains data related to the wavebank, such as the identifiers (a cue) of a sound and where to find the sound inside the wavebank. The application file is the project file for the tool you installed earlier. The tool can build the application file into a wave bank and sound bank.

### Code

When we wish to play a sound we can use the following Lua code:

```lua
local cue = "Lead"
local bank = "KingOfTheHill"
local sound = Sound({Cue = cue, Bank = bank})
PlaySound(sound)
```

These names are important to remember as we'll see them inside the tool.

### Tool

When you open up the tool a lot of information is thrown at you. For the sake of this guide we'll give a name to some of the UI elements.

![](images/interface.png)

On the toolbar navigate towards `File -> Open project` and choose `KingOfTheHill.xap` that is inside the example folder. You'll notice that the hiearchy is more populated than before - you can ignore the majority of it.

Inside the hierarchy click twice on `Wave banks -> KingoftheHill`. This opens up the following window:

![](images/wavebank.png)

As mentioned earlier a wavebank contains the audio files. For the sake of the guide we'll reproduce this and make a new bank that holds the same information. Inside the hierarchy right click on `Wave banks` and choose `New Wave Bank`. I'll call this wavebank `KingOfTheHillAlt`.

This opens up a new window, this time devoid of entries. Right click on the new window and select `Insert Wave file(s)...`. Navigate towards `repository/example/wav` and select all the audio files inside that folder. Once imported you'll notice one difference: all the entries are marked red instead of green. This doesn't mean something is wrong: it means they're not used (yet).

In order to use them we'll need a sound bank. We'll first inspect the old sound bank. Inside the hierchy click twice on `Sound banks -> KingOfTheHill`. If you select `Exp-all` in both tables then you'll have a view similar to:

![](images/soundbank.png)

For the sake of this guide we'll give a name to some of the UI elements.

We'll start with the sound table. This conveys what tracks can be played and what audio files are part of the track in question. Each entry in the sound table has a priority. This tells the game that if there are many sound elements running which ones it should prioritize. A track is a selection of various audio files. For each audio file there is a percentage in its name: this is how likely this audio file will be chosen. When there are multiple files you can adjust its weight accordingly in the property window.

Lets create our own sound bank. Inside the hierarchy right click on `Sound banks` and choose `New Sound Bank`. I'll call this sound bank `KingOfTheHillAlt`.

This opens up a new window, this time devoid of entries. Select the wave bank we previously made. Drag and drop the audio file `Exp-All` into the sound table. A few things happen:
 - A new entry is added to the sound table.
 - A new track is added with an event 'Play wave' containing the wave file we selected previously.
 - The wave file in the wave bank is green instead of red.

Now drag `Exp-All-Egg` not onto the sound table, but as a child of the `Play Wave` node in the tree hierarchy. A few things happen:
 - A new audio file is added to the `Play wave` event in our track.
 - The wave file in the wave bank is green instead of red.

You can adjust the weight as to how often each sound should play in the properties window. You can do so by selecting the wave file in the sound tree hierachy and adjusting its weight in the property window. You can also adjust the priority of an entry in the sound table. A voice over (VO) should have the highest priority as you do not want players to miss your announcements during a battle.

And last but not least we need a cue. Drag the `Exp-All` entry inside the sound table into the cue table. And there we have it. Lets look back at the code we saw earlier:

```lua
local cue = "Lead"
local bank = "KingOfTheHill"
local sound = Sound({Cue = cue, Bank = bank})
PlaySound(sound)
```

The information that we need to pass it along is more intuitive. The cue is the information that tells what track should be played. A track determines what audio file should be played. The bank determines where we can find the information for the cue, track and the audio file respectively.

### Building it

Once you've added in all your audio files in your wave bank and used them in your sound bank via sounds, tracks and cues it is time to build it so that we can use it in our game. On the toolbar navigate to `File -> Build`. This opens up a small window. Press `finish` to finalize the build. When we look at the folder our project file (.xap) is inside of we see two new folders:

![](images/build.png)

We're interested in the windows version. Inside you'll find the the files we need.

### Installing it

When you want to use these sounds in your own map or mod you'll need to put it where the game expects it. In both cases you need to add a `sounds` folder to the root folder of your map or mod. Note that the folder is case sensitive, do **not** use 'Sounds'!

As an example for a map:

![](images/place-map.png)

As an example for a mod:

![](images/place-mod.png)

You can then copy the output files (.xwb and .xsb) to this folder. Make sure to restart Supreme Commander if the files are new. If they are only replaced then it is sufficient to just restart the map you are testing on. 

You can check whether you installed it correct by going through the moholog. At the very top it should say something similar to 'Found sound files in %MAP%', where %MAP% is your map. You can open up the moholog with F9 is by adding `/showlog` as a program argument.

### Using it

#### Using sounds in maps

Sounds are played on the UI side of the game. Therefore we need to pass the sound to the UI side. This can be accomplished with the following code:

```lua
local cue = "Exp-All"
local bank = "KingOfTheHill"
local sound = {Cue = cue, Bank = bank}
table.insert(Sync.Sounds, sound) -- Sync is available in global scope
```

In turn the sound is played automatically on the UI side. You can find the file responsible for this in [lua/UserSync.lua](https://github.com/FAForever/fa/blob/deploy/fafdevelop/lua/UserSync.lua#L19) which runs the following code:

```lua
function OnSync()

    -- (...)

    --Play Sounds
    for k, v in Sync.Sounds do
        PlaySound(Sound{ Bank=v.Bank, Cue=v.Cue })
    end

    -- (...)

end
```

#### Using sounds in mods

Sounds in mods take the same road from your code to your ears. Somehow you need to inform the UI side what sound you wish to play. Maps cannot hook the `OnSync()` function shown in the previous example, but mods can. And with that you could for example play a sound for only one player.

There are a few examples:
 - [Sending over the sound](https://gitlab.com/supreme-commander-forged-alliance/mods/king-of-the-hill/-/blob/master/mods/King%20of%20the%20Hill/modules/sim-utils.lua#L24)
 - [Receiving the sound on the UI side](https://gitlab.com/supreme-commander-forged-alliance/mods/king-of-the-hill/-/blob/master/mods/King%20of%20the%20Hill/hook/lua/UserSync.lua#L5)
 - [Playing the sound](https://gitlab.com/supreme-commander-forged-alliance/mods/king-of-the-hill/-/blob/master/mods/King%20of%20the%20Hill/modules/controllerUI.lua#L104)

### What else is there?

A lot - you can chance the pitch, cue and other properties. You can click on things and inspect the properties window to see what is possible.

## FAQ

### Is .wav the only supported value?

Yes. You can use software such as [Audacity](https://www.audacityteam.org/) to convert it accordingly. 

### I've got noise in my sound: what to do!

Uhh - Yes. You can use software such as [Audacity](https://www.audacityteam.org/) to adjust the sound files where neccesary. I do not provide assistance with this: there is sufficient information to find on the internet. 

Typically: if you have noise or other artifacts then you should probably use different files or re-record it without the noise or artifacts.